﻿using System.Collections.Generic;
using Microsoft.Extensions.Options;

namespace UnitOfMeasurement.Models
{
    public class uom
    {
        public string name { get; set; }
        public string annotation { get; set; }
        public List<string> conversionFormula { get; set; }
        public List<string> quantities { get; set; }
        public string baseunit { get; set; }
    }
}