﻿using Microsoft.AspNetCore.DataProtection;

namespace UnitOfMeasurement
{
    public class JsonObjectRequest
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public double Val { get; set; }
        public string FromUom { get; set; }
        public string ToUom { get; set; }
        public string InputParameter { get; set; }
    }
}