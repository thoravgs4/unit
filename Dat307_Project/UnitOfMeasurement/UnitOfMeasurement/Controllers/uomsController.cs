﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using UnitOfMeasurement.Infrastructure;

namespace UnitOfMeasurement.Controllers
{
    
    [ApiController]
    [Route("[controller]")]
    public class uomsController : ControllerBase
    {
        private readonly ILogger<uomsController> _logger;
        private readonly TablesTableUom _tablesTable;

        public uomsController(ILogger<uomsController> logger)
        {
            _logger = logger;
            _tablesTable = new TablesTableUom();
        }


        [HttpGet("Quantity")]
        public ActionResult Get()
        {
            //var q = new TablesUom();

            return Ok(_tablesTable.Quantitiese());
        }
        
        [HttpGet("Dimension")]
        public ActionResult Dimension()
        {
            return Ok(_tablesTable.Dimension());
        }


        [HttpPost("UOMQType")]
        [Produces("application/json")]
        [DataType("json")]
        public ActionResult UpdateProductAsync(JsonObjectRequest product)
        {
           
            var q = product.InputParameter;

            return Ok(_tablesTable.UOM_QType(q));
        }
        
        [HttpPost("UOMDClass")]
        [Produces("application/json")]
        [DataType("json")]
        public ActionResult GetUomForDimensionClass(JsonObjectRequest product)
        {
            var q = product.InputParameter;

            return Ok(_tablesTable.UOM_GClass(q));
        }
        [HttpPost("unitConversion")]
        [Produces("application/json")]
        [DataType("json")]
        public ActionResult unitConversion(JsonObjectRequest product)
        {
            var q = product.Val;
            var from = product.FromUom;
            var toUom = product.ToUom;
            
            return Ok(_tablesTable.Conversion(q,from,toUom));
        }
    }
}