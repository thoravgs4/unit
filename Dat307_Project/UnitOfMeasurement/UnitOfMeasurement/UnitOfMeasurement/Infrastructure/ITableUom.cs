﻿using System.Collections.Generic;
using UnitOfMeasurement.Models;

namespace UnitOfMeasurement.Infrastructure
{
    public interface ITableUom
    {
        List<string> getDimensionList();
        List<string> getQuantityTypes();
        List<uom> UOM_GClass(string GivenClass);
        object Conversion(double val, string fromUom, string toUom);
        public List<uom> UOM_QType(string QType);
        void load();
    }
}