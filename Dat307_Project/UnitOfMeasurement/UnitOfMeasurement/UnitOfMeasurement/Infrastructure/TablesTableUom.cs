﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using Microsoft.AspNetCore.Mvc;
using UnitOfMeasurement.Models;

namespace UnitOfMeasurement.Infrastructure
{
    public class TablesTableUom : ITableUom
    {
        private readonly Dictionary<string, List<uom>> dimensionClass; // <dimension,<baseunit,list<annotation>>
        private XElement ListElements; // upload xml file
        private readonly Dictionary<string, List<uom>> quantity; // <quantity,dimension>
        public Dictionary<string, uom> uoms;
        private int loadCount = 0;


        public TablesTableUom()
        {
            quantity = new Dictionary<string, List<uom>>();
            dimensionClass = new Dictionary<string, List<uom>>();
            uoms = new Dictionary<string, uom>();
            if (loadCount == 0)
            {
                load(); 
            }
            
        }

        public void load()
        {
            ListElements = XElement.Load("eng.xml");
            var elementsByParties = from item in ListElements.Descendants("UnitOfMeasure")
                select item;
            var xElements = elementsByParties as XElement[] ?? elementsByParties.ToArray();

            foreach (var uomBlock in xElements)
            {
                // var enumerable = uom.Element("UnitOfMeasure");
                var uomName = uomBlock.Element("Name")?.Value;
                var annotationUom = uomBlock.Attribute("annotation")?.Value;
                var q = uomBlock.Element("QuantityType")?.Value; // save the quantity name
                var dimension = uomBlock.Element("DimensionalClass")?.Value; // save the dimension name 
                var catalogSymbol = uomBlock.Element("CatalogSymbol")?.Value; // save the catalog symbol
                var baseUnit =
                    uomBlock.Element("ConversionToBaseUnit")?.Attribute("baseUnit")?.Value; //save the base unit
                var numerator = uomBlock.Element("ConversionToBaseUnit")?.Element("Fraction")?.Element("Numerator")
                    ?.Value;
                var denominator = uomBlock.Element("ConversionToBaseUnit")?.Element("Fraction")?.Element("Denominator")
                    ?.Value;
                var fraction = uomBlock.Element("ConversionToBaseUnit")?.Element("Factor")?.Value;


                if (dimension != null)
                {
                    if (baseUnit == null)
                    {
                        var temp = new uom
                        {
                            name = uomName,
                            baseunit = catalogSymbol,
                            conversionFormula = new List<string> {"1", "1"},
                            annotation = annotationUom
                        };
                        uoms[annotationUom] = temp; // unit of measure list


                        // todo: check if the dimension class is registered in the dictionary
                        if (dimensionClass.ContainsKey(dimension))
                        {
                            //if the key is in the dictionary add the 
                            dimensionClass[dimension].Add(temp);
                        }
                        else
                        {
                            var templist = new List<uom>();
                            templist.Add(temp);
                            dimensionClass[dimension] = templist;
                        }
                    }
                    else
                    {
                        if (fraction == null)
                        {
                            var temp = new uom
                            {
                                name = uomName,
                                baseunit = baseUnit,
                                conversionFormula = new List<string> {numerator, denominator},
                                annotation = annotationUom
                            };

                            dimensionClass[dimension].Add(temp);
                            uoms[annotationUom] = temp;
                        }
                        else
                        {
                            var temp = new uom
                            {
                                name = uomName,
                                baseunit = baseUnit,
                                conversionFormula = new List<string> {fraction}, // B/c and c = 1.
                                annotation = annotationUom
                            };
                            dimensionClass[dimension].Add(temp);
                            uoms[annotationUom] = temp;
                        }
                    }
                }

                if (q != null)
                {
                    if (annotationUom == "gu") continue;
                    var temp = uoms[annotationUom];
                    // the key is the name of the quantity and the key is the dimension the quantity belongs
                    if (quantity.ContainsKey(q))
                    {
                        // if the quantity key is in the dictionary then no need to create new. just add the uom in the list.
                        quantity[q].Add(temp);
                    }
                    else
                    {
                        // if not found create new key and list value for the uom and add the uom.
                        var templist = new List<uom>();
                        templist.Add(temp);
                        quantity[q] = templist;
                    }
                }
            }
        }


        public List<string> getDimensionList()
        {
            var dimension = dimensionClass.Keys.ToList();
            //var json = JsonSerializer.Serialize(quantitylistT);

            return dimension;
        }


        public List<string> getQuantityTypes()
        {
            var quantitylistT = quantity.Keys.ToList();
            //var json = JsonSerializer.Serialize(quantitylistT);

            return quantitylistT;
        }

        /**
         * This block returns the uom for a given dimension class*
         * @param name of 1/(dimension symbol)
         */
        //List all unit of measurement for a given class
        public List<uom> UOM_GClass(string GivenClass)
        {
            var list = dimensionClass[GivenClass].ToList();
            // var json = JsonSerializer.Serialize(list);
            // var j = new
            //{
            /*listOFunits = list
        };
        

        return new JsonResult(j)
        {
            Value = j
        }.Value;*/
            return list;
        }


        public object Conversion(double val, string fromUom, string toUom)
        {
            var fuom = uoms[fromUom];
            var tuom = uoms[toUom];
            var resultuomAnnotation = tuom.annotation;
            var resultUomName = tuom.name;
            var fconversionFormula = fuom.conversionFormula;
            var tconversionFormula = tuom.conversionFormula;
            double B1, C1, B2, C2, z = 0.0;


            if (uoms[fromUom] == null || uoms[toUom] == null)
                //return "Conversion between the units is impossible";

                return new JsonResult("Nothing is happening Now !!");

            if (fconversionFormula.Count == 2)
            {
                var num = fconversionFormula[0];
                B1 = double.Parse(num);

                C1 = double.Parse(fconversionFormula[1]);
            }
            else
            {
                B1 = double.Parse(fconversionFormula[0], CultureInfo.InvariantCulture);
                C1 = 1.0;
            }

            if (fconversionFormula.Count == 2)
            {
                B2 = double.Parse(tconversionFormula[0], CultureInfo.InvariantCulture);
                C2 = double.Parse(tconversionFormula[0], CultureInfo.InvariantCulture);
            }
            else
            {
                B2 = double.Parse(tconversionFormula[0], CultureInfo.InvariantCulture);
                C2 = 1.0;
            }

            if (fuom.baseunit.Equals(tuom.baseunit))
                z = B1 / C1 / (B2 / C2) * val;
            else
                // return "Conversion between the units is impossible";
                return new JsonResult("Nothing is happening Now !!");

            var numer = new
            {
                result = z,
                uom = resultUomName,
                annotation = resultuomAnnotation
            };


            // string js = JsonSerializer.Serialize(numer);

            //var nu= new JsonResult(numer);


            // return new JsonResult(numer);
            return new JsonResult(numer)
            {
                Value = numer
            }.Value;
        }


        public List<uom> UOM_QType(string QType)
        {
            var list = quantity[QType].ToList();
            // var json = JsonSerializer.Serialize(list);


            return list;
        }
    }
}