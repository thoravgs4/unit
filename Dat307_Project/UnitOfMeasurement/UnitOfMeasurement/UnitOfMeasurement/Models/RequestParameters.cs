﻿namespace UnitOfMeasurement
{
    public class RequestParameters
    {
        public double Value { get; set; }
        public string FromUom { get; set; }
        public string ToUom { get; set; }
        public string DimensionInput { get; set; } //an input value of dimension
        public string QuantityInput { get; set; } //an input value of quantity
    }
}