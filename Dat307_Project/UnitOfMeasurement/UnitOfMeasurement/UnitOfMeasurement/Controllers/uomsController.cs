﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using UnitOfMeasurement.Infrastructure;

namespace UnitOfMeasurement.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class uomsController : ControllerBase
    {
        private readonly ILogger<uomsController> _logger;
        private readonly TablesTableUom _tablesTable;

        public uomsController(ILogger<uomsController> logger)
        {
            _logger = logger;
            _tablesTable = new TablesTableUom();
        }


        [HttpGet("Quantity")]
        public ActionResult Quantity()
        {
            //var q = new TablesUom();

            return Ok(_tablesTable.getQuantityTypes());
        }

        [HttpGet("Dimension")]
        public ActionResult Dimension()
        {
            return Ok(_tablesTable.getDimensionList());
        }


        [HttpPost("UOMQType")]
        [Produces("application/json")]
        [DataType("json")]
        public ActionResult UpdateProductAsync(RequestParameters product)
        {
            var q = product.QuantityInput;

            return Ok(_tablesTable.UOM_QType(q));
        }

        [HttpPost("UOMDClass")]
        [Produces("application/json")]
        [DataType("json")]
        public ActionResult GetUomForDimensionClass(RequestParameters product)
        {
            var q = product.DimensionInput;

            return Ok(_tablesTable.UOM_GClass(q));
        }

        [HttpPost("unitConversion")]
        [Produces("application/json")]
        [DataType("json")]
        public ActionResult unitConversion(RequestParameters product)
        {
            var q = product.Value;
            var from = product.FromUom;
            var toUom = product.ToUom;

            return Ok(_tablesTable.Conversion(q, from, toUom));
        }
    }
}