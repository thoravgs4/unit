﻿using System.Collections.Generic;
using System.Xml.Linq;

namespace UnitOfMeasurement.Infrastructure
{
    public interface ITableUom
    {
        string Dimension();
        string Quantitiese();
        string UOM_GClass(string GivenClass);
        string Conversion(double val,string fromUom,string toUom );
        string UOM_QType(string QType);
        void load();
    }
}