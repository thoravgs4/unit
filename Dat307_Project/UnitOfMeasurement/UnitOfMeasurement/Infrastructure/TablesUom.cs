﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace UnitOfMeasurement.Infrastructure
{
    public class TablesUom : IUOM
    {
        public Dictionary<string, Dictionary<string, string>> dataLagring;
        private XElement ListElements; // upload xml file
        private Dictionary<string, string> quantity; // <quantity,dimension>
        private Dictionary<string, string> dimensionClass; // <dimension,baseunit>


        public TablesUom()
        {
            quantity = new Dictionary<string, string>();
            load();
        }

        public void load()
        {
            ListElements = XElement.Load("eng.xml");
        }

        //Vi har fått problem , vi kan fikse neste gang .
        //Dimension dictionary klages
        public string Dimension()
        {
            var elementsByParties = from item in ListElements.Descendants("UnitOfMeasure")
                select item;
            var xElements = elementsByParties as XElement[] ?? elementsByParties.ToArray();

            foreach (var uom in xElements)
            {
                // var enumerable = uom.Element("UnitOfMeasure");
                var dimension = uom.Element("DimensionalClass")?.Value; //returb alle dimension class in list
                var catalogSymbol = uom.Element("CatalogSymbol")?.Value; // fetch quantity name (inner value).
                var baseUnit = uom.Element("ConversionToBaseUnit")?.Attribute("baseUnit")?.Value;
             //Checker if the key is null . Denne dictionary , dimension er key. 
                if (dimension != null)
                {
                    dimensionClass[dimension] = catalogSymbol;

                    if (baseUnit == null)
                    {
                        dimensionClass[dimension] = catalogSymbol;
                    }
                    else
                        dimensionClass[dimension] = baseUnit;
                }
                
            }

            var quantitylistT = dimensionClass.Keys.ToArray();
                var json = JsonConvert.SerializeObject(quantitylistT, Formatting.Indented);


                return json;
            }


            public string Quantitiese()
            {
                var elementsByParties = from item in ListElements.Descendants("UnitOfMeasure")
                    select item;
                var xElements = elementsByParties as XElement[] ?? elementsByParties.ToArray();

                foreach (var uom in xElements)
                {
                    // var enumerable = uom.Element("UnitOfMeasure");
                    var dimension = uom.Element("DimensionalClass")?.Value;
                    var q = uom.Element("QuantityType")?.Value; // fetch quantity name (inner value).
                    //var uom_qType = uom.Element("SameUnit")?.Attribute("uom")?.Value;

                    if (q != null)
                    {
                        quantity[q] = dimension;
                    }
                }

                var quantitylistT = quantity.Keys.ToArray();
                var json = JsonConvert.SerializeObject(quantitylistT, Formatting.Indented);


                return json;
            }

            //List all unit of measurement for a given class
            public string UOM_GClass(string GivenClass)
            {
                throw new NotImplementedException();
            }

            public string ConvertToFrom()
            {
                throw new NotImplementedException();
            }

            public string UOM_QType(string QType)
            {
                var type = Quantitiese();

                var dic = new Dictionary<string, string>();


                var obj = JObject.Parse(type);
                // var name = (Array) obj;

                foreach (var item in obj)
                {
                    if (item.Value.ToString() == QType)

                        dic[item.Key] = item.Value.ToString();
                }


                var json = JsonConvert.SerializeObject(dic, Formatting.Indented);


                return json;
            }
        }
    }