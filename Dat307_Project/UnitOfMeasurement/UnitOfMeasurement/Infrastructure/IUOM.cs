﻿using System.Collections.Generic;
using System.Xml.Linq;

namespace UnitOfMeasurement.Infrastructure
{
    public interface IUOM
    {
        string Dimension();
        string Quantitiese();
        string UOM_GClass(string GivenClass);
        string ConvertToFrom();
        string UOM_QType(string QType);
        void load();
    }
}