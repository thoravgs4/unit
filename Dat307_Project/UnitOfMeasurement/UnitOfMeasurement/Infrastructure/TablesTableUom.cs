﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.Json;
using UnitOfMeasurement.Models;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace UnitOfMeasurement.Infrastructure
{
    public class TablesTableUom : ITableUom
    {
        private XElement ListElements; // upload xml file
        private Dictionary<string, List<uom>> quantity; // <quantity,dimension>
       

        private Dictionary<string, List<uom>> dimensionClass; // <dimension,<baseunit,list<annotation>>
        public Dictionary<string, uom> uoms;


        public TablesTableUom()
        {
            quantity = new Dictionary<string, List<uom>>();
            dimensionClass = new Dictionary<string, List<uom>>();
            uoms = new Dictionary<string, uom>();
            load();
        }

        public void load()
        {
            ListElements = XElement.Load("eng.xml");
            var elementsByParties = from item in ListElements.Descendants("UnitOfMeasure")
                select item;
            var xElements = elementsByParties as XElement[] ?? elementsByParties.ToArray();

            foreach (var uomBlock in xElements)
            {
                // var enumerable = uom.Element("UnitOfMeasure");
                var uomName = uomBlock.Element("Name")?.Value;
                var annotationUom = uomBlock.Attribute("annotation")?.Value;
                var q = uomBlock.Element("QuantityType")?.Value; // save the quantity name
                var dimension = uomBlock.Element("DimensionalClass")?.Value; // save the dimension name 
                var catalogSymbol = uomBlock.Element("CatalogSymbol")?.Value; // save the catalog symbol
                var baseUnit =
                    uomBlock.Element("ConversionToBaseUnit")?.Attribute("baseUnit")?.Value; //save the base unit
                var numerator = uomBlock.Element("ConversionToBaseUnit")?.Element("Factor")?.Element("Numerator")
                    ?.Value;
                var denominator = uomBlock.Element("ConversionToBaseUnit")?.Element("Factor")?.Element("Denominator")
                    ?.Value;
                var fraction = uomBlock.Element("ConversionToBaseUnit")?.Element("Factor")?.Value;
               



               if (dimension != null)
                {
                    
                    if (baseUnit == null)
                    {
                        var temp = new uom()
                        {
                            name = uomName,
                            baseunit = catalogSymbol,
                            conversionFormula = new List<string>() {"1","1"},
                            annotation = annotationUom,
                            
                            
                        };
                        uoms[annotationUom]= temp; // unit of measure list


                        // todo: check if the dimension class is registered in the dictionary
                        if (dimensionClass.ContainsKey(dimension))
                        {
                            //if the key is in the dictionary add the 
                            dimensionClass[dimension].Add(temp);
                        }
                        else
                        {
                            var templist = new List<uom>();
                            templist.Add(temp);
                            dimensionClass[dimension] = templist;
                        }
                    }
                    else
                    {
                        if (fraction == null)
                        {
                            var temp = new uom()
                            {
                                name = uomName,
                                baseunit = baseUnit,
                                conversionFormula = new List<string>() {numerator, denominator},
                                annotation = annotationUom,
                             

                            };
                            
                            dimensionClass[dimension].Add(temp); 
                            uoms[annotationUom]= temp;
                        }
                        else
                        {
                            var temp = new uom()
                            {
                                name = uomName,
                                baseunit = baseUnit,
                                conversionFormula = new List<string>() {fraction}, // B/c and c = 1.
                                annotation = annotationUom,

                            };
                            dimensionClass[dimension].Add(temp);
                            uoms[annotationUom]= temp;

                        }
                        
                    }
                    
                }
               
                if (q != null )
                {
                    if (annotationUom == "gu") continue;
                    var temp = uoms[annotationUom];
                    // the key is the name of the quantity and the key is the dimension the quantity belongs
                    if (quantity.ContainsKey(q))
                    {
                        // if the quantity key is in the dictionary then no need to create new. just add the uom in the list.
                        quantity[q].Add(temp); 

                    }
                    else
                    {
                        // if not found create new key and list value for the uom and add the uom.
                        var templist = new List<uom>();
                        templist.Add(temp);
                        quantity[q] = templist; 
                    }

                }
            }
        }


        public string Dimension()
        {
            var quantitylistT = dimensionClass.Keys;
            var json = JsonSerializer.Serialize(quantitylistT);

            return json;
        }


        public string Quantitiese()
        {
            var quantitylistT = quantity.Keys.ToArray();
            var json = JsonSerializer.Serialize(quantitylistT);

            return json;
        }
        
        /**
         * This block returns the uom for a given dimension class*
         * @param name of 1/(dimension symbol)
         */
        //List all unit of measurement for a given class
        public string UOM_GClass(string GivenClass)
        {
            var list = dimensionClass[GivenClass].ToArray();
            var json = JsonSerializer.Serialize(list);
            
            
            
            return json;
        }

        public string Conversion(double val,string fromUom,string toUom )
        {
            var fuom = uoms[fromUom];
            var tuom = uoms[toUom];
            var resultuomAnnotation = tuom.annotation;
            var resultUomName = tuom.name;
            var fconversionFormula = fuom.conversionFormula;
            var tconversionFormula = tuom.conversionFormula;
            double B1,C1,B2,C2,z = 0.0;
            if (fconversionFormula.Count == 2)
            {
                B1 = double.Parse(fconversionFormula[0]);
                C1 = double.Parse(fconversionFormula[1]);

            }
            else
            {
                B1 = double.Parse(fconversionFormula[0]);
                C1 = 1.0;
            }
            if (fconversionFormula.Count == 2)
            {
                B2 = double.Parse(tconversionFormula[0]);
                C2 = double.Parse(tconversionFormula[1]);

            }
            else
            {
                B2 = double.Parse(tconversionFormula[0]);
                C2 = 1.0;
            }

            if (fuom.baseunit.Equals(tuom.baseunit))
            {
                z = ((B1 / C1) / (B2 / C2)) * val;
            }
            else
            {
                return "Conversion between the units is impossible";
            }
            

            var jason = new List<string>(new string[]
            {
                $"result:{z}", $"uom:{resultUomName}", $"Annotation:{resultuomAnnotation}"
            });
            
            var js = JsonSerializer.Serialize(jason);
            


            return js;
        }

        /**
         * This block returns all unit of measurement to a given quantity type
         * @param is annotation of the Quantity type*
         */
        public string UOM_QType(string QType)
        {
            var list = quantity[QType].ToArray();
            var json = JsonSerializer.Serialize(list);


            return json;
        }
    }
}